package com.test.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.text);

        ApiService.getData("03.06.2019")
                .enqueue(new Callback<RequestModel>() {
                    @Override
                    public void onResponse(Call<RequestModel> call, Response<RequestModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            RequestModel.ExchangeRate rate = response.body().getExchangeRate().get(1);
                            String formattedRate = String.format(Locale.US, "Rate of %s is %.2f", rate.getCurrency(), rate.getSaleRateNB());
                            text.setText(formattedRate);
                        } else {
                            Toast.makeText(MainActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestModel> call, Throwable t) {
                        Toast.makeText(MainActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
